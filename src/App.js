import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Home from "./components/HomePage/HomePage"
import Extraction from "./components/Extraction/Extraction"
import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <div className="App">
          <Route exact path='/' component={Home} />
          <Route exact path='/extraction' component={Extraction} />
          {/* <Home /> */}
        </div>
      </Switch>
    </Router>
  )
}

export default App;

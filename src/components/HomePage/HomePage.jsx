import React, { useEffect } from "react"
import { createStyles, makeStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"
import Divider from "@material-ui/core/Divider"
import { Typography } from "@material-ui/core"
import Drawer from "@material-ui/core/Drawer"
import CssBaseline from "@material-ui/core/CssBaseline"
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew"
import IconButton from "@material-ui/core/IconButton"
import Button from "@material-ui/core/Button"
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            display: "flex",
            flexDirection: "column"
        },
        appBar: {
            backgroundColor: "#2F3D5C",
            zIndex: theme.zIndex.drawer + 1
        },
        toolbar: {
            height: "50px"
        },
        logoutButton: {
            color: "white",
            textAlign: "right"
        },
        version: {
            marginRight: "10px"
        }
    })
)

export default function HomePage() {
    const classes = useStyles()
    // const [version, setVersion] = React.useState("")
    return (
        <div className={classes.root}>
            <div>
                <Link to="/extraction">
                    <Button
                        variant="contained"
                        aria-label="contained primary"
                        color="primary"
                    // onClick={() => {
                    //     history.push("/Extraction")
                    // }}
                    >
                        CREATE CONVERSION WORKFLOW
                    </Button>
                </Link>
            </div>
        </div >
    )
}
import React, { useEffect } from "react"
import { createStyles, makeStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"
import Divider from "@material-ui/core/Divider"
import { Typography } from "@material-ui/core"
import Grid from "@material-ui/core/Grid"
import FormControl from "@material-ui/core/FormControl"
import TextField from "@material-ui/core/TextField"
import Paper from "@material-ui/core/Paper"
import CreateIcon from "@material-ui/icons/Create"
import ClearIcon from "@material-ui/icons/Clear"
import Drawer from "@material-ui/core/Drawer"
import CssBaseline from "@material-ui/core/CssBaseline"
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew"
import IconButton from "@material-ui/core/IconButton"
import Button from "@material-ui/core/Button"

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            marginTop: "50px",
            marginLeft: "50px",
            height: "auto",
            width: "800px",
            backgroundColor: "#f2f2f2"
        },
        headerText: {
            padding: "12px",
            marginLeft: "10px"
        },
        formControl: {
            margin: "1.5% 0% !important",
            width: "100%"
        },
        targetForm: {
            marginTop: "1%",
            paddingLeft: "2%"
        },
        formButton: {
            marginRight: "10px !important"
        }
    })
)

export default function Extraction() {
    const classes = useStyles()
    return (
        <div>
            <Paper className={classes.root}>
                <Typography className={classes.headerText} variant="h5">
                    Extraction
                </Typography>
                <Divider />
                <div className={classes.targetForm}>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <FormControl className={classes.formControl}>
                                <TextField
                                    id="name"
                                    label="Name"
                                    placeholder="name"
                                    fullWidth
                                    required
                                    variant="outlined"
                                    margin="normal"
                                    size="small"
                                    InputLabelProps={{
                                        shrink: true
                                    }}
                                    InputProps={{
                                        autoComplete: "off"
                                    }}
                                    size="small"
                                />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <TextField
                                    id="spl_file"
                                    label="SPL File"
                                    placeholder="SPL file"
                                    fullWidth
                                    required
                                    variant="outlined"
                                    margin="normal"
                                    size="small"
                                    InputLabelProps={{
                                        shrink: true
                                    }}
                                    InputProps={{
                                        autoComplete: "off"
                                    }}
                                    size="small"
                                />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <TextField
                                    id="target_siem"
                                    label="Target SIEM"
                                    placeholder="target SIEM"
                                    fullWidth
                                    required
                                    variant="outlined"
                                    margin="normal"
                                    size="small"
                                    InputLabelProps={{
                                        shrink: true
                                    }}
                                    InputProps={{
                                        autoComplete: "off"
                                    }}
                                    size="small"
                                />
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <div className={classes.formControl}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    size="medium"
                                    className={classes.formButton}
                                    startIcon={<CreateIcon />}
                                >
                                    Create
                                    </Button>
                                <Button
                                    variant="contained"
                                    size="medium"
                                    className={classes.formButton}
                                    startIcon={<ClearIcon />}
                                >
                                    Clear
                                    </Button>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </Paper >
        </div >
    )
}